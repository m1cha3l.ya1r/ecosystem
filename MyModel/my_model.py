import sys
import time
from datetime import datetime
from graphics.dashboard import *
import sqlite3
from utilities import *

sys.path.append('../')


def main():
    # animals_to_db = []
    # Mega trial loop
    start_model_run = datetime.now()
    for trial in range(0, N_TRIALS):
        start_model = datetime.now()
        print("Trial", trial)

        # initiate population at t = 0
        animals = start_population()
        food_location = create_food_per_step(FOOD_PER_TIME_STEP, ARENA_SIZE)
        food_created_daily = []
        food_eaten = []
        # ecosystem snapshot collector
        animals_dict_list = []
        # Run ecosystem
        for d in range(0, N_DAYS):
            for t in TIMESTEPS:
                # data collection for the dashboard
                if t == TIMESTEPS[0]:
                    available_food = []
                    food_created_daily = []
                    timing = []
                for animal in animals:
                    # basal metabolism per time step
                    animal.energy -= DAILY_ENERGY_METABOLISM / N_TIMESTEPS
                    # aging per time step
                    animal.age += 1 / N_TIMESTEPS
                    animal.dead = True if animal.age > LIFESPAN - 1e-5 or animal.energy <= 1e-5 else False
                    # update moving points per objet to the current time step
                    animal.compute_moves_per_step(t, PROFILE_STATIC_PHASE[animal.species])
                # create a list of animals with moving points > 0 per time step
                movable_animals = update_movable_animals_list(animals)

                # calculate the amount of food in the system
                if FOOD_COMPETITION:    # and d < 500:
                    # values for competition on food resources:
                    food_created_increase = create_food_per_step(FOOD_PER_TIME_STEP, ARENA_SIZE)
                    food_location = food_location + food_created_increase
                else:
                    # values for NO competition on food resources:
                    food_created_increase = np.array([])
                    food_location = np.ones(ARENA_SIZE, dtype=int)*1000

                # as long as there are movable animals, let them eat and move to the new locations
                # start = time.time()
                while len(movable_animals) > 0:
                    move_to = randint(0, ARENA_SIZE, len(movable_animals))
                    # move each relevant animal in 1 step
                    for i, m in enumerate(movable_animals):
                        # mean locations for k nearest
                        m.move(move_to[i])
                    food_location = animals_eating_by_food_location(t, animals, food_location)
                    # update the movable animals list to contain only those who have left moves
                    movable_animals = update_movable_animals_list(animals)

                food_created_daily.append((t, sum(food_created_increase)))
                ################################################################################################
                shuffle(animals)
                # reproduction
                # t0 = time.time()
                selected_pairs = random_pairs(animals)
                # timing.append(time.time() - t0)
                # if t == max(TIMESTEPS):
                #     print(sum(timing))
                # print(f'random_pairs: {time.time() - t0}')
                # t0 = time.time()
                newborns = engagement(animals, t, selected_pairs, P_NEWBORN)
                # print(f'mating: {time.time() - t0}')
                animals = animals + newborns
                shuffle(animals)
                ################################################################################################
                # Timestep-level records
                available_food.append((t, sum(food_location)))
                ################################################################################################
                # save data
                # t0 = time.time()

                if SAVE_TO_SQLITE_DB:
                    save_to_db(cur, con, animals, trial, d, t, food_created_increase, food_location)
                    # if d % 100 == 0 and t == max(TIMESTEPS):
                    #     data.append([animals, trial, d, t, food_created_increase, food_location])
                    #     save_to_db(cur, con, data)
                    #     data = []
                    # else:
                    #     data.append([animals, trial, d, t, food_created_increase, food_location])
                # print(f'save to db: {time.time() - t0}')
                ################################################################################################
                # high density and activity hours encourage individuals to leave the habitat
                animals_count = len(clear_the_dead(animals))
                if animals_count > objects_count:
                    animals = dispersal(animals, 96, np.pi / 4., 100)
                # limit the quantity on the habitat
                animals_count = len(clear_the_dead(animals))
                if animals_count > MAX_ANIMALS:
                    animals = choice(animals, MAX_ANIMALS)
                    # print('population cutoff')
                # print(f'time: {t:7.4f}, '
                #       f'A_count: {len([a for a in animals if a.species == "A"]):3}, '
                #       f'B_count: {len([a for a in animals if a.species == "B"]):3}, '
                #       f'A_<1_energy: {len([a.energy for a in animals if a.species == "A" and a.energy <= 1]):3}, '
                #       f'B_<1_energy: {len([a.energy for a in animals if a.species == "B" and a.energy <= 1]):3}, '
                #       f'A_dead_count: {len([a for a in animals if a.species == "A" and a.dead])}, '
                #       f'B_dead_count: {len([a for a in animals if a.species == "B" and a.dead])}, '
                #       f'A_eaten: {np.sum([a.food_eaten for a in animals if a.species == "A"]):5.2f}, '
                #       f'B_eaten: {np.sum([a.food_eaten for a in animals if a.species == "B"]):5.2f}, '
                #       f'A_new: {len([a for a in newborns if a.species == "A"])}, '
                #       f'B_new: {len([a for a in newborns if a.species == "B"])}, '
                #       f'A_mean_age: {np.mean([a.age for a in animals if a.species == "A"]):5.3f}, '
                #       f'B_mean_age: {np.mean([a.age for a in animals if a.species == "B"]):5.3f}, '
                #       )

                # remove all dead animals
                animals = clear_the_dead(animals)
            #  Show figures for live updates
            A_count = len([i for i in animals if i.species == 'A'])
            B_count = len([i for i in animals if i.species == 'B'])
            if d % 10 == 0:
                print(f'model run duration: {datetime.now() - start_model_run}, '
                      f'trial: {trial}, '
                      f'day: {d}, '
                      f'A: {A_count}, '
                      f'B: {B_count}, '
                      f'food available: {np.sum(food_location):.2f}, '
                      f'food created: {np.sum([f[1] for f in food_created_daily]):.2f}, '
                      f'new born: {len([i for i in animals if i.age<=1]):.2f}, '
                      f'elders: {len([i for i in animals if i.age>=LIFESPAN-1]):.2f}, '
                      f'mean energies: {np.mean([i.energy for i in animals]):.2f}, '
                      f'mean age: {np.mean([i.age for i in animals]):.2f}, '
                      f'mean phase A: {np.mean([i.phase for i in animals if i.species=="A"]):.2f}, '
                      f'mean phase B: {np.mean([i.phase for i in animals if i.species == "B"]):.2f}, '
                      )
            if A_count == 0 or B_count == 0:
                break
            # elif d % 50 == 0:
            #     dashboard(animals, available_food, food_created_daily, d)
            #     plt.savefig(f'figs/test/dashboard_trial_{trial}_day_{str(d).zfill(4)}.jpeg',
            #                 dpi=100, bbox_inches='tight')
            #     plt.close('all')


if '__main__' in __name__:
    start = datetime.now()
    if SAVE_TO_SQLITE_DB:
        con = sqlite3.connect(DB_FILE)
        cur = con.cursor()
        cur.execute("DROP TABLE IF EXISTS animals")
        cur.execute('CREATE TABLE IF NOT EXISTS animals ('
                    'trial integer, '
                    'day integer, '
                    'time real, '
                    'species text, '
                    'loc integer, '
                    'phase real, '
                    'phase_penalty real, '
                    'energy real, '
                    'food_eaten real, '
                    'age real, '
                    'moves_per_step real, '
                    'dead integer, '
                    'fitness integer, '
                    'extinction integer)')
        cur.execute("DROP TABLE IF EXISTS food")
        cur.execute('CREATE TABLE IF NOT EXISTS food ('
                    'trial integer, '
                    'day integer, '
                    'time real, '
                    'food_created real, '
                    'food_available real)'
                    )

    main()
    end = datetime.now()
    print(f'start: {start}, end: {end}, total time: {end-start}')
