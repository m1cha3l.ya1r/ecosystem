from numpy import pi, mean, linspace, cos, array
from numba import njit
from datetime import datetime

SAVE_TO_SQLITE_DB = True
# model type
FOOD_COMPETITION = True
# if True, allows inter species pairs too, if False only intra species allowed
REPRODUCTIVE_INTERFERENCE = False
# calculate the phase part probability to mate through a "phases distance"
FILTER_PHASES_BY_DISTANCE = False

# environment
N_DAYS = 2001   # 25000
N_TRIALS = 100    # 20
ARENA_SIZE = 100
N_TIMESTEPS = 24    # 10
FOOD_PER_TIME_STEP = 4

# daily energy consumed:
ENERGY_PER_MOVE = 1. / N_TIMESTEPS
ENERGY_TO_CHILD = 1. / 3.
DAILY_ENERGY_METABOLISM = 0.5

# population characteristics
SPECIES_ALL = ('A', 'B')
ENERGY_INIT = 10.    # 10
AMP_INIT = dict(zip(SPECIES_ALL, (0, 0)))
phase_init = 0.0
PHASE_INIT = dict(zip(SPECIES_ALL, (phase_init, -phase_init)))
NEWBORN_PHASE_SD = pi/16
r = 0.5
POP_RATIOS = dict(zip(SPECIES_ALL, (r, 1-r)))
objects_count = 120
N_ANIMALS_INIT = dict(zip(SPECIES_ALL, tuple([int(POP_RATIOS[i]*objects_count) for i in SPECIES_ALL])))
LIFESPAN = 20
SENSING_PRESENCE_DISTANCE = ARENA_SIZE #// 20
# essential for reproduction interference to limit the level of interference
# probability to be part of the current community in sigma units, the higher it is, less communities are possible
# value > PI means bypass
PHASE_SYNC_RANGE = 4. * NEWBORN_PHASE_SD

# mating
# tuple of the probability to get a successful new born per species
P_SUCCESSFUL_NEWBORN = (2./N_TIMESTEPS, 2./N_TIMESTEPS)
#
P_OPPOSITE_GENDERS = (0.5, 0.5)
# average of 1 newborn per day per couple, only if other conditions met, in practice - less
P_NEWBORN = dict(zip(SPECIES_ALL, tuple(array(P_OPPOSITE_GENDERS) * array(P_SUCCESSFUL_NEWBORN))))
MIN_ENERGY_FOR_MATING = 0
# mutation probability:
MUTANT_PROB = 0.

# limit of inhabited occupation
MAX_ANIMALS = 300
# model steps structure
MOVES_PER_DAY = N_TIMESTEPS
half_spacing = pi/N_TIMESTEPS
TIMESTEPS = linspace(-pi+half_spacing, pi-half_spacing, N_TIMESTEPS)
t = 0


# action profiles
act_profile_txt = '$N_{moves}\\cdot\\frac{1+cos(\\alpha+\pi)}{{2 \cdot \pi}}$'
"""
act_profile_shape_txt = \ 
    '$\\frac{1}{\\sigma\cdot\\sqrt{2\cdot\pi}}\cdot e^{-\\frac{1}{2}\cdot(\\frac{x-\\mu}{\\sigma})^2}$\n' \
    '$\\mu=0, \\sigma=0.8$'
"""


@njit()
def action_profile_shape(alpha):
    p = (1 + cos(alpha))/(2*pi)
    return p
    # mu = 0
    # sigma = 0.8
    # return np.exp(-((alpha-mu)/sigma)**2/2)/(sigma*np.sqrt(2*np.pi))


static_phases = (0.0, 0.0)
PROFILE_STATIC_PHASE = dict(zip(SPECIES_ALL, static_phases))
ACT_PROFILE_SHAPE = dict(zip(SPECIES_ALL,
                             [action_profile_shape(TIMESTEPS + PROFILE_STATIC_PHASE[i]) for i in SPECIES_ALL]))
# shape sum (~1)
SHAPE_SUM = dict(zip(SPECIES_ALL,
                     [2*pi*sum(ACT_PROFILE_SHAPE[i])/N_TIMESTEPS for i in SPECIES_ALL]))
# daily activity profile
ACT_PROFILE = dict(zip(SPECIES_ALL,
                       [ACT_PROFILE_SHAPE[i] * MOVES_PER_DAY for i in SPECIES_ALL]))
MEAN_ACTIVITY = dict(zip(SPECIES_ALL,
                         [mean(ACT_PROFILE[i]) for i in SPECIES_ALL]))

now = datetime.now()
if SAVE_TO_SQLITE_DB and FOOD_COMPETITION:
    DB_FILE = f'competition_{objects_count}_{now.strftime("%Y%m%d-%H%M")}.db'
elif SAVE_TO_SQLITE_DB and REPRODUCTIVE_INTERFERENCE:
    DB_FILE = f'interference_{objects_count}_{now.strftime("%Y%m%d-%H%M")}.db'