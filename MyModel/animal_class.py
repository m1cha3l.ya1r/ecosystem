from config import *
from numpy.random import uniform, randint, normal
from numpy import ndenumerate, sqrt
from numba.experimental import jitclass
from numba.core.types import int32, float64, string, boolean


@njit()
def startup_location():
    return randint(ARENA_SIZE)


spec = [
    ('loc', int32),
    ('species', string),
    ('phase', float64),
    ('phase_penalty', float64),
    ('energy', float64),
    ('food_eaten', float64),
    ('age', float64),
    ('moves_per_step', float64),
    ('dead', boolean),
    ('fitness', int32),
    ('extinction', int32)
]


@jitclass(spec)
class Animal:
    loc: int
    species: str
    phase: float
    phase_penalty: float
    energy: float
    food_eaten: float
    age: float
    moves_per_step: float
    dead: bool
    fitness: int
    extinction: int

    def __init__(self, species, phase, energy, age=0):
        self.loc = startup_location()
        self.species = species
        self.phase = phase
        # self.phase_penalty = (1 - abs(self.phase / pi))
        # self.phase_penalty = np.sqrt(1 - abs(self.phase / pi))
        # self.phase_penalty = np.cos(self.phase)
        self.phase_penalty = sqrt(1 - (self.phase/pi)**2)
        self.energy = energy
        self.food_eaten = 0
        self.age = age
        self.moves_per_step = 0
        self.dead = False
        self.fitness = 0
        self.extinction = 0

    def move(self, new_loc):
        # move to new location,
        if self.energy > 1e-5:
            self.energy = self.energy - ENERGY_PER_MOVE
            self.loc = new_loc
            self.moves_per_step = self.moves_per_step - 1
        else:
            self.dead = True
            self.moves_per_step = 0
        self.dead = True if self.energy <= 1e-5 else False

    def compute_moves_per_step(self, time_step, static_phase):
        if self.energy > 1e-5:
            # calculate number of moves per model iteration (step):
            moves_in_step = self.phase_penalty * (MOVES_PER_DAY *
                                                  action_profile_shape(self.phase + static_phase + time_step))
            self.moves_per_step = int(moves_in_step + randint(0, 2))
        else:
            self.dead = True
            self.moves_per_step = 0

    def eat(self, time_step, food, static_phase):
        energy_gained = food * self.phase_penalty * action_profile_shape(self.phase + static_phase + time_step)
        self.energy += energy_gained
        self.food_eaten = food

    def sex(self, mate):
        if uniform(0, 1) < MUTANT_PROB:
            child_phase = uniform(-pi/2, pi/2)
        else:
            mu_phase = (self.phase + mate.phase)/2.
            child_phase = normal(mu_phase, NEWBORN_PHASE_SD)
        if child_phase < -pi:
            child_phase = 2*pi + child_phase
        elif child_phase > pi:
            child_phase = child_phase - 2*pi

        child_energy = self.energy * ENERGY_TO_CHILD + \
                       mate.energy * ENERGY_TO_CHILD

        child = Animal(self.species,
                       child_phase,
                       child_energy,
                       0)

        return child
