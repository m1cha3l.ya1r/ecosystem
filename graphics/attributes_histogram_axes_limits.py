import numpy as np
from MyModel.config import *

attributes_axis_limits = {
    'phase':
        lambda ax: (
            ax.set_xlim(-np.pi, np.pi),
            ax.set_ylim(0, 10),
            ax.set_title(f'self phase histogram, last day'),
            ax.set_xlabel('phase')
        ),
    'amp':
        lambda ax: (
            ax.set_xlim(-0.2, 0.5),
            ax.set_ylim(0, 12),
            ax.set_title(f'amp histogram, last day'),
            ax.set_xlabel('amp')
        ),
    'age':
        lambda ax: (
            # ax.set_xlim(0, 12),
            # ax.set_ylim(0, 5),
            ax.set_title(f'age histogram, last day'),
            ax.set_xlabel('age [days]')
        ),
    'food_eaten':
        lambda ax: (
            ax.set_xlim(0, 5),
            ax.set_ylim(0, 15),
            ax.set_title(f'food eaten histogram, last day'),
            ax.set_xlabel('food eaten')
        ),
    'energy':
        lambda ax: (
            # ax.set_xlim(0, 5),
            # ax.set_ylim(0, 15),
            ax.set_title(f'energy histogram, last day')

        ),
    'loc':
        lambda ax: (
            ax.set_xlim(-1, ARENA_SIZE),
            ax.set_ylim(0, 4),
            ax.set_title(f'location histogram, last day'),
            ax.set_xlabel('location')
        )
}