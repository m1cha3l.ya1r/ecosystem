This is an agent based model to describe 2 (or more) populations leaving in the same ecosystem.
<br>
It is inspired from a paper by [Vance Difan Gao](https://onlinelibrary.wiley.com/doi/full/10.1002/ece3.6770).
